# Adds installer to desktop file search path in liveos
if systemd-analyze condition ConditionKernelCommandLine=carbon.liveos &> /dev/null; then
    XDG_DATA_DIRS="@datadir@/gdm/initial-setup:${XDG_DATA_DIRS}"
    export XDG_DATA_DIRS
fi