units_dir = get_option('prefix') / get_option('libdir') / 'systemd' / 'user'

# App resources
app_resources = gnome.compile_resources('app-resources', 'setup.gresource.xml')

# Install app icon & desktop entry
configure_file(
	input: 'carbon-setup.desktop',
	output: 'sh.carbon.setup.desktop',
	configuration: conf_data,
	install_dir: get_option('datadir') / 'gdm' / 'initial-setup' / 'applications'
)
install_data(
	'icons/carbon-setup.svg',
	rename: 'sh.carbon.setup.svg',
	install_dir: get_option('datadir') / 'icons' / 'hicolor' / 'scalable' / 'apps'
)
install_data(
	'icons/carbon-setup-symbolic.svg',
	rename: 'sh.carbon.setup-symbolic.svg',
	install_dir: get_option('datadir') / 'icons' / 'hicolor' / 'symbolic' / 'apps'
)

# Polkit config
configure_file(
	input: 'polkit.policy',
	output: 'sh.carbon.setup.policy',
	configuration: conf_data,
	install_dir: get_option('datadir') / 'polkit-1' / 'actions'
)
configure_file(
	input: 'polkit.rules',
	output: 'sh.carbon.setup.rules',
	configuration: conf_data,
	install_dir: get_option('datadir') / 'polkit-1' / 'rules.d'
)

# systemd-sysusers config
install_data(
	'sysusers.conf',
	rename: 'carbon-setup.conf',
	install_dir: get_option('libdir') / 'sysusers.d'
)

# gnome-session config
configure_file(
	input: 'initial-setup.desktop',
	output: 'sh.carbon.setup.greeter.desktop',
	configuration: conf_data,
	install_dir: get_option('datadir') / 'gdm' / 'greeter' / 'applications'
)
install_data(
	'gnome-session.conf',
	rename: 'gnome-initial-setup.session',
	install_dir: get_option('datadir') / 'gnome-session' / 'sessions'
)
install_data(
	'systemd-session.conf',
	rename: 'session.conf',
	install_dir: units_dir / 'gnome-session@gnome-initial-setup.target.d'
)
configure_file(
	input: 'carbon-setup.service',
	output: 'carbon-setup.service',
	configuration: conf_data,
	install_dir: units_dir
)

# gnome-shell modes
install_data(
	'gnome-shell.json',
	rename: 'initial-setup.json',
	install_dir: get_option('datadir') / 'gnome-shell' / 'modes'
)

# settings overrides
install_data(
	'gsettings-overrides.conf',
	rename: '01-carbon-setup.gschema.override',
	install_dir: get_option('datadir') / 'glib-2.0' / 'schemas'
)

# profile.d scripts
configure_file(
	input: 'profile.sh',
	output: 'carbon-setup.sh',
	configuration: conf_data,
	install_dir: get_option('sysconfdir') / 'profile.d',
	install_mode: 'rwxr-xr-x'
)

# Set up the copy helper
configure_file(
	input: 'carbon-setup-copy-helper.service',
	output: 'carbon-setup-copy-helper.service',
	configuration: conf_data,
	install_dir: units_dir
)
configure_file(
	input: 'carbon-setup-copy-helper.desktop',
	output: 'carbon-setup-copy-helper.desktop',
	configuration: conf_data,
	install_dir: get_option('sysconfdir') / 'xdg' / 'autostart'
)
meson.add_install_script('../build-aux/meson-add-wants.sh', units_dir,
 	'gnome-session.target.wants/', 'carbon-setup-copy-helper.service'
)
