# Russian translations for carbon-setup package.
# Copyright (C) 2021 THE carbon-setup'S COPYRIGHT HOLDER
# This file is distributed under the same license as the carbon-setup package.
# Automatically generated <>, 2021-2022.
#
msgid ""
msgstr ""
"Project-Id-Version: carbon-setup\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-08-07 02:52-0400\n"
"PO-Revision-Date: 2022-08-07 03:07-0400\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Gtranslator 42.0\n"

#. TRANSLATORS: The display name of the demo user. Use whatever word
#. demonstrates that most clearly; doesn't have to be "demo"
#: src/main.vala:106
msgid "Demo"
msgstr "Проба"

#: src/pages/disclaimer/disclaimer.vala:35
msgid "Disclaimer"
msgstr "Отказ от Ответственности"

#. TRANSLATORS: The %s will be replaced with the name of the OS
#: src/pages/disclaimer/disclaimer.vala:37
#, c-format
msgid ""
"This is an early development build of %s. Please be aware that breaking "
"changes may still occur. You may need to manually migrate data after such a "
"change. We provide no stability or security guarantees. Back up all "
"important data. Do not use in a production environment."
msgstr ""
"Это ранняя разработка %s. Имейте в виду, что критические изменения все еще "
"могут произойти. После такого изменения вам может потребоваться перенести "
"данные вручную. Мы не даем никаких гарантий стабильности или безопасности. "
"Сделайте резервную копию всех важных данных. Не используйте в "
"производственной среде."

#: src/pages/done/done.vala:19
msgid "Almost Done…"
msgstr "Почти Готово…"

#: src/pages/done/done.vala:20
msgid "You're all set up!"
msgstr "Все готово!"

#: src/pages/done/done.vala:22
msgid "Your computer needs to restart to finish the setup"
msgstr "Ваш компьютер должен перезагрузиться, чтобы завершить настройку"

#: src/pages/done/done.vala:23
msgid "Your computer is now ready to be used"
msgstr "Ваш компьютер готов к работе"

#: src/pages/done/done.vala:25
msgid "_Reboot"
msgstr "_Перезагрузить"

#. TRANSLATORS: This button completes setup and takes the user to
#. their new desktop
#: src/pages/done/done.vala:28
msgid "_Done"
msgstr "_Готово"

#: src/pages/done/done.vala:42
msgid "Reboot"
msgstr "Перезагрузить"

#: src/pages/drive-select/drive-select.vala:31
msgid "Select a Drive"
msgstr "Выберите Диск"

#: src/pages/drive-select/drive-select.vala:32
msgid ""
"All of the data will be erased from the drive you select. Make sure you have "
"any important data backed up."
msgstr ""
"Все данные будут удалены с выбранного вами диска. Убедитесь, что у вас есть "
"резервные копии важных данных."

#. TRANSLATORS: This line tells the user no drives were detected. The
#. "found" language here doesn't indicate a search
#: src/pages/drive-select/drive-select.vala:35
msgid "No Drives Found"
msgstr "Нет Дисков"

#: src/pages/drive-select/drive-select.vala:36
msgid "Are you sure?"
msgstr "Вы уверены?"

#. TRANSLATORS: Confirm that the user is ok with the drive being wiped
#. and start the installation of the OS
#: src/pages/drive-select/drive-select.vala:39
msgid "Co_ntinue"
msgstr "_Продолжить"

#: src/pages/drive-select/drive-select.vala:40
msgid "_Cancel"
msgstr "_Отмена"

#. TRANSLATORS: Format is the name of a disk drive, for example:
#. "WD Blue" or "Samsung 840 EVO"
#: src/pages/drive-select/drive-select.vala:145
#, c-format
msgid "All of the apps and data on this <b>%s</b> will be erased!"
msgstr "Все приложения и данные на этом <b>%s</b> будут удалены!"

#: src/pages/hostname/hostname.vala:17
msgid "Name this Device"
msgstr "Назовите это Устройство"

#: src/pages/hostname/hostname.vala:18
msgid ""
"This is used to identify this device when communicating with other devices "
"nearby and on the network."
msgstr ""
"Это используется для идентификации этого устройства при обмене данными с "
"другими устройствами поблизости и в сети."

#. TRANSLATORS: This describes what the window is/does. Not a command.
#. The %s will be replaced with the name of the OS
#: src/pages/installer-welcome/installer-welcome.vala:13
#, c-format
msgid "Install %s"
msgstr "Установить %s"

#. TRANSLATORS: The %s will be replaced with the name of the OS
#: src/pages/installer-welcome/installer-welcome.vala:15
#, c-format
msgid "%s will be installed onto this device"
msgstr "%s будет установлен на это устройство"

#. TRANSLATORS: Install step: creating filesystem layout
#: src/pages/install-progress/install-progress.vala:31
msgid "Initializing…"
msgstr "Инициализация…"

#. TRANSLATORS: Install step: Copying the OS
#: src/pages/install-progress/install-progress.vala:36
msgid "Copying Files…"
msgstr "Копирование файлов…"

#. TRANSLATORS: Install step: copying w/ percent complete
#: src/pages/install-progress/install-progress.vala:45
#, c-format
msgid "Copying Files… (%d%%)"
msgstr "Копирование файлов… (%d%%)"

#. TRANSLATORS: Install step: merging OS and kernel into final image
#: src/pages/install-progress/install-progress.vala:51
msgid "Merging…"
msgstr "Объединение…"

#. TRANSLATORS: Install step: Turning the image into a bootable OS
#: src/pages/install-progress/install-progress.vala:56
msgid "Deploying…"
msgstr "Развертывание…"

#. TRANSLATORS: Install step: deleting temporary files
#: src/pages/install-progress/install-progress.vala:61
msgid "Cleaning up…"
msgstr "Уборка…"

#. TRANSLATORS: Install step: Wiping disk & creating partitions
#: src/pages/install-progress/install-progress.vala:89
msgid "Formatting…"
msgstr "Форматирование…"

#. TRANSLATORS: Install step: Installing the bootloader
#: src/pages/install-progress/install-progress.vala:139
msgid "Installing bootloader…"
msgstr "Установка загрузчика…"

#. TRANSLATORS: Install step: Shutting down disk we just installed to
#: src/pages/install-progress/install-progress.vala:160
msgid "Finalizing…"
msgstr "Завершение…"

#: src/pages/kbd-layout/kbd-layout-row.ui:42
msgid "Preview"
msgstr "Предпросмотр"

#: src/pages/kbd-layout/kbd-layout.vala:21
msgid "Keyboard"
msgstr "Клавиатура"

#: src/pages/kbd-layout/kbd-layout.vala:22
msgid "Test your settings here"
msgstr "Проверьте свои настройки здесь"

#. TRANSLATORS: Found here means that the user searched for a
#. keyboard layout that isn't in the list.
#: src/pages/kbd-layout/kbd-picker.vala:229
msgid "No Keyboard Layouts Found"
msgstr "Раскладки клавиатуры не найдены"

#: src/pages/kbd-layout/kbd-picker.vala:230
#: src/pages/language/lang-picker.vala:185
msgid "Search"
msgstr "Поиск"

#. TRANSLATORS: Found here means that the user searched for a
#. language that isn't in the list.
#: src/pages/language/lang-picker.vala:184
msgid "No Languages Found"
msgstr "Языки Не Найдены"

#: src/pages/language/lang-picker.vala:186
msgid "More…"
msgstr "Более…"

#. TRANSLATORS: This should be a warm, welcoming message. Someething
#. you'd say to greet someone at the door
#: src/pages/language/language.vala:28
msgid "Welcome!"
msgstr "Добро Пожаловать!"

#: src/pages/network/network.vala:46 src/pages/network/network.vala:49
msgid "Connect to Wi-Fi"
msgstr "Подключитесь к Wi-Fi"

#: src/pages/network/network.vala:47
msgid "Hidden Network…"
msgstr "Скрытая Сеть…"

#: src/pages/network/network.vala:48
msgid "You're Already Connected"
msgstr "Вы Уже Подключены"

#: src/pages/network/network.vala:50
msgid "No Network Devices"
msgstr "Нет Сетевых Устройств"

#: src/pages/network/network.vala:51
msgid ""
"No available network devices were found. You can continue setting up your "
"device without an internet connection."
msgstr ""
"Нет доступных сетевых устройств. Вы можете продолжить настройку устройства "
"без подключения к Интернету."

#: src/pages/network/network.vala:52
msgid "Plug in your Ethernet"
msgstr "Подключите свой Ethernet"

#: src/pages/network/network.vala:53
msgid ""
"For a better experience, connect your device to the network via Ethernet. "
"You can continue setting up your device without an internet connection."
msgstr ""
"Для удобства, подключите устройство к сети через Ethernet. Вы можете "
"продолжить настройку устройства без подключения к Интернету."

#. TRANSLATORS: This is shown on the wifi selection page if the
#. device is already connected to ethernet
#: src/pages/network/network.vala:100
msgid "You're already connected via Ethernet."
msgstr "Вы уже подключены через Ethernet."

#. TRANSLATORS: This is shown on the wifi selection page if the
#. device is already connected to a mobile carrier network
#: src/pages/network/network.vala:107
msgid ""
"You're already connected to a mobile network. Connect to Wi-Fi for a better "
"experience."
msgstr ""
"Вы уже подключены к мобильной сети. Подключитесь к Wi-Fi для лучшего "
"удобства."

#. TRANSLATORS: This is shown on the wifi selection page if the
#. device has an ethernet port but is not connected to it
#: src/pages/network/network.vala:119
msgid "You can also connect via Ethernet."
msgstr "Вы также можете подключиться через Ethernet."

#: src/pages/network/network.vala:159
#: src/pages/try-or-install/try-or-install.vala:52
msgid "Feature isn't implemented"
msgstr "Функция не реализована"

#: src/pages/network/network.vala:160
msgid "Setup cannot connect to hidden networks yet"
msgstr "Программа установки пока не может подключиться к скрытым сетям"

#: src/pages/password/password.vala:19
msgid "Set a Password"
msgstr "Выберите Пароль"

#: src/pages/password/password.vala:20
msgid "Password"
msgstr "Пароль"

#. TRANSLATORS: Tells user to enter their password again to confirm it
#: src/pages/password/password.vala:22
msgctxt "Password"
msgid "Confirm"
msgstr "Подтвердите"

#: src/pages/theme/theme.vala:22
msgid "Appearance"
msgstr "Внешний вид"

#: src/pages/theme/theme.vala:23
msgid "Default"
msgstr "По умолчанию"

#: src/pages/theme/theme.vala:24
msgid "Dark"
msgstr "Темный"

#: src/pages/timezone/timezone.vala:61
msgid "Timezone"
msgstr "Часовой Пояс"

#: src/pages/timezone/timezone.vala:62
msgid "Search for your city to set the clock"
msgstr "Найдите свой город, чтобы настроить часы"

#: src/pages/timezone/timezone.vala:63
msgid "Is this the correct time?"
msgstr "Сейчас правильное время?"

#: src/pages/timezone/timezone.vala:64
msgid "No"
msgstr "Нет"

#: src/pages/timezone/timezone.vala:65
msgid "Yes"
msgstr "Да"

#. The %Z format sometimes spits out ugly offsets. Let's
#. make them nicer
#. TRANSLATORS: UTC here means Coordinated Universal Time.
#. the %:z will be replaced by the offset (i.e. UTC+06:00)
#: src/pages/timezone/timezone.vala:212
msgid "UTC%:z"
msgstr "UTC%:z"

#: src/pages/try-or-install/try-or-install.vala:21
msgid "Try or Install?"
msgstr "Попробовать или Установить?"

#. TRANSLATORS: "Try" here means "Try carbonOS and see if you like it"
#. It's a demo or trial of the system
#: src/pages/try-or-install/try-or-install.vala:25
msgid "_Try"
msgstr "_Попробовать"

#: src/pages/try-or-install/try-or-install.vala:26
msgid ""
"Changes will not be saved. Your existing data will not be modified. "
"Performance and features may be limited"
msgstr ""
"Изменения не сохранятся. Ваши существующие данные не будут изменены. "
"Производительность и функции могут быть ограничены"

#: src/pages/try-or-install/try-or-install.vala:28
msgid "_Install"
msgstr "_Установить"

#. TRANSLATORS: The %s will be replaced with the name of the OS
#: src/pages/try-or-install/try-or-install.vala:30
#, c-format
msgid "Wipe the disk and install a fresh copy of %s"
msgstr "Стереть диск и установить свежую копию %s"

#: src/pages/try-or-install/try-or-install.vala:32
msgid "_Repair"
msgstr "_Отремонтировать"

#. TRANSLATORS: The %s will be replaced with the name of the OS
#: src/pages/try-or-install/try-or-install.vala:34
#, c-format
msgid "Repair an existing installation of %s"
msgstr "Отремонтировать существующую установку %s"

#. TRANSLATORS: The %s will be replaced with the name of the OS
#: src/pages/try-or-install/try-or-install.vala:54
#, c-format
msgid "Setup cannot repair %s installations yet"
msgstr "Программа установки пока не может отремонтировать установки %s"

#: src/pages/user/user.vala:26
msgid "Create an Account"
msgstr "Сделать Аккаунт"

#: src/pages/user/user.vala:27
msgid "Full Name"
msgstr "Полное Имя"

#: src/pages/user/user.vala:28
msgid "Username"
msgstr "Имя Пользователя"

#: src/user.vala:91
msgid "Username is too long"
msgstr "Имя пользователя слишком длинное"

#: src/user.vala:94
msgid ""
"Username must only contain letters, digits, and the following characters: - _"
msgstr ""
"Имя пользователя должно содержать только буквы, цифры и следующие символы: - "
"_"

#: src/user.vala:97
msgid "Username must start with a letter"
msgstr "Имя пользователя должно начинаться с буквы"

#: src/user.vala:100
msgid "This will be used to name your home folder and can't be changed"
msgstr ""
"Это будет использоваться для имени вашей домашней папки и не может быть "
"изменено"

#: src/window.vala:32
msgid "_Skip"
msgstr "_Пропустить"

#: src/window.vala:33
msgid "_Next"
msgstr "_Дальше"

#: src/window.vala:34
msgid "_Back"
msgstr "_Назад"

#~ msgid "We're glad you like it!"
#~ msgstr "Мы рады, что вам понравилась система!"

#~ msgid "_Begin"
#~ msgstr "_Начинать"
