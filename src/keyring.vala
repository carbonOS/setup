// Functions to properly handle the GNOME keyring
namespace Setup.Keyring {
    private const string DUMMY_PASSWORD = "carbon-setup";
    private static bool gkd_failed = false;

    [DBus(name="org.gnome.keyring.InternalUnsupportedGuiltRiddenInterface")]
    private interface KeyringInternal : DBusProxy {
        public abstract void change_with_master_password(string collection,
            Variant old_value, Variant new_value) throws Error;
    }

    // Ensures that gnome-keyring is running & initialized
    void ensure_running() {
        if (Driver.mock) {
            print("Keyring: ensure_running\n");
            return;
        }

        // Start the daemon
        Subprocess subprocess;
        try {
            subprocess = new Subprocess(SubprocessFlags.STDIN_PIPE |
                SubprocessFlags.STDOUT_PIPE | SubprocessFlags.STDERR_SILENCE,
                "gnome-keyring-daemon", "--unlock");
        } catch (Error e) {
            critical("Failed to spawn gnome-keyring-daemon: %s", e.message);
            gkd_failed = true;
            return;
        }

        // Pipe the dummy password into the keyring
        try {
            subprocess.communicate_utf8(DUMMY_PASSWORD, null, null, null);
        } catch (Error e) {
            critical("Failed to unlock gnome-keyring-daemon: %s", e.message);
            gkd_failed = false;
        }
    }

    // Sets the keyring password to be consistent with the new user's password
    async void set_password(string password) {
        if (Driver.mock) {
            print("Keyring: ChangeWithMasterPassword\n");
            return;
        }

        if (gkd_failed) {
            warning("Keyring failed to initialize. Not setting keyring password");
            return;
        }

        // Encode the secret values
        var old_secret = new Secret.Value(DUMMY_PASSWORD, DUMMY_PASSWORD.length,
            "text/plain");
        var new_secret = new Secret.Value(password, password.length,
            "text/plain");

        // Set the password
        try {
            var secret = Secret.Service.get_sync(Secret.ServiceFlags.OPEN_SESSION);
            var proxy = Bus.get_proxy_sync<KeyringInternal>(BusType.SESSION,
                "org.gnome.keyring", "/org/freedesktop/secrets",
                DBusProxyFlags.DO_NOT_AUTO_START);
            proxy.change_with_master_password(
                "/org/freedesktop/secrets/collection/login",
                secret.encode_dbus_secret(old_secret),
                secret.encode_dbus_secret(new_secret));
        } catch (Error e) {
            critical("Failed to set keyring password: %s", e.message);
        }
    }
}
