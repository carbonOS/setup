using Linux;
using Posix;

// USAGE: carbon-setup-mount-helper ROOT ESP
int main(string[] args) {
    if (args.length < 3) error("not enough arguments");
    var root_dev = args[1], esp_dev = args[2];
    int r;

    // Mount the rootfs
    r = mount(root_dev, "/mnt", "btrfs");
    if (r < 0) error("Failed to mount root from %s: %m", root_dev);

    // Make the boot directory
    r = mkdir("/mnt/boot", 0755);
    if (r < 0 && errno != EEXIST) error("Failed to create ESP mountpoint: %m");

    // Format the ESP as FAT32
    // We do this here because UDisks will format the ESP as FAT16. The UEFI spec
    // only requires FAT16 support on removable drives, so it's possible that
    // there's a firmware out there somewhere that will not work properly with
    // a FAT16-formatted internal drive. We follow the spec and format as FAT32
    string[] format_cmd = { "mkfs.vfat", "-F32", "-n", "EFI", esp_dev };
    var format = new Subprocess.newv(format_cmd, SubprocessFlags.NONE);
    try {
        format.wait_check();
    } catch {
        error("Failed to format the ESP at %s", esp_dev);
    }

    // Mount the ESP at the boot directory
    r = mount(esp_dev, "/mnt/boot", "vfat");
    if (r < 0) error("Failed to mount ESP from %s: %m", esp_dev);

    return 0;
}
