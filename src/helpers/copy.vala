using Posix;
using FileAttribute;

// Files to copy over
const string[] FILES_TO_MOVE = {
    ".config/dconf/user", // gsettings storage
    ".config/goa-1.0/accounts.conf", // online accounts
    ".local/share/keyrings/login.keyring", // passwords

    // stamp that we finished initial-setup
    // should come last to make sure we don't mark it early
    ".config/gnome-initial-setup-done"
};

bool is_owned_by_us(File file) {
    try {
        var info = file.query_info(UNIX_UID, FileQueryInfoFlags.NOFOLLOW_SYMLINKS);
        var uid = info.get_attribute_uint32(UNIX_UID);
        return uid == geteuid();
    } catch (Error e) {
        warning("Failed to query ownership of %s: %s", file.get_path(), e.message);
        return false;
    }
}

void move_file(File src_dir, File dest_dir, string path) throws Error {
    var src = src_dir.get_child(path);
    var dest = dest_dir.get_child(path);

    // Make target dir
    try {
        dest.get_parent().make_directory_with_parents();
    } catch (IOError.EXISTS ignored) {
        // It's OK
    }

    // Move the file
    try {
        src.move(dest, FileCopyFlags.NONE);
    } catch (IOError.NOT_FOUND ignored) {
        // It's OK'
    }
}

int main(string[] args) {
    // Find our homedir
    var home = File.new_for_path(Environment.get_home_dir());

    // Find g-i-s homedir
    unowned var gis_pw = getpwnam("gnome-initial-setup");
    if (gis_pw == null) error("Failed to find gis user");
    var gis_home = File.new_for_path(gis_pw.pw_dir);
    if (!gis_home.query_exists() || !is_owned_by_us(gis_home))
        return EXIT_SUCCESS;

    // Copy the files
    foreach (var filename in FILES_TO_MOVE) {
        try {
            move_file(gis_home, home, filename);
        } catch (Error e) {
            warning("Failed to move %s: %s", filename, e.message);
        }
    }

    return EXIT_SUCCESS;
}
