namespace Setup {
    enum Mode {
        INSTALLER_GREETER, // OS Installer + "Do you want to try or install?"
        INSTALLER, // OS Installer
        INITIAL_SETUP; // First-boot experience
    }

    class Driver : Adw.Application {
        public Mode mode;
        public Window shell;
        private Page[] pages;

        private Greeter greeter;
        public UDisks.Client udisks;

        public NewUser? new_user = null;
        public UDisks.Drive? target_drive = null;
        public string? language;

        public static bool mock;

        public Driver(Mode mode) {
            Object(application_id: "sh.carbon.setup");
            this.mode = mode;

            var mode_set = Environment.get_variable("SETUP_MODE") != null;
            var force_off = Environment.get_variable("SETUP_NO_MOCK") != null;
            mock = mode_set && !force_off;

            // Register actions for the reboot notification
            var reboot_action = new SimpleAction("reboot", null);
            reboot_action.activate.connect(() => {
                this.reboot.begin();
            });
            this.add_action(reboot_action);
            var focus_action = new SimpleAction("focus", null);
            focus_action.activate.connect(() => {
                this.shell.present_with_time(0);
            });
            this.add_action(focus_action);
        }

        public override void startup() {
            base.startup();

            // Initialize the shell
            this.shell = new Window(mode == Mode.INSTALLER);
            shell.populate_text();

            // Initialize various services as-needed
            Kbd.init();
            if (mode != Mode.INSTALLER)
                this.greeter = new Greeter();
            if (mode != Mode.INITIAL_SETUP) {
                try {
                    this.udisks = new UDisks.Client.sync();
                } catch (Error e) {
                    error("Failed to initialize udidks: %s", e.message);
                }
            } else { // mode == Mode.INITIAL_SETUP
                //TODO Keyring.ensure_running();
            }

            // Populate the shell based on the mode
            pages = make_pages();
            foreach (var page in pages) shell.add_page(page);

            // Visually show if the app is in "mock" mode
            if (mock) this.shell.add_css_class("devel");
        }

        public override void activate() {
            // Show the window
            this.add_window(this.shell);
            this.shell.show();
        }

        public async void finish_setup_new_user() {
            // Create the user
            yield new_user.create();

            // Update the GNOME keyring password
            //TODO: yield Keyring.set_password(new_user.password);

            // Pre-authenticate the new user so that we log in immediately
            // after the user presses "Done"
            yield greeter.auth(new_user.user_name, new_user.password);

            // Notify GDM of the UID of the user we just created
            try {
                var path = Path.build_filename(Environment.get_home_dir(),
                                               "gnome-initial-setup-uid");
                FileUtils.set_contents(path, "%u".printf(new_user.uid));
            } catch (FileError e) {
                warning("Failed to stamp g-i-s-uid: %s", e.message);
            }

            // Create the done stamp file
            try {
                var path = Path.build_filename(Environment.get_user_config_dir(),
                                               "gnome-initial-setup-done");
                FileUtils.set_contents(path, "yes");
            } catch (FileError e) {
                warning("Failed to stamp g-i-s-done: %s", e.message);
            }
        }

        public void start_user_session() {
            greeter.start_session(false);
            this.quit();
        }

        public async void start_live_session() {
            // Create the LiveOS user
            var new_user = new NewUser();
            new_user.user_name = "live";
            // TRANSLATORS: The display name of the demo user. Use whatever word
            // demonstrates that most clearly; doesn't have to be "demo"
            new_user.real_name = _("Demo");
            new_user.password = null;
            new_user.language = language;
            yield new_user.create();

            // Log into the LiveOS user
            yield greeter.auth("live", null);
            greeter.start_session(true);
            this.quit();
        }

        public async void reboot() {
            if (mock) {
                print("Driver: reboot\n");
                return;
            }

            try {
                var conn = yield Bus.get(BusType.SYSTEM);
                yield conn.call("org.freedesktop.login1",
                    "/org/freedesktop/login1", "org.freedesktop.login1.Manager",
                    "Reboot", new Variant("(b)", true), null,
                    DBusCallFlags.NONE, -1);
            } catch (Error e) {
                critical("Failed to reboot: %s", e.message);
            }

            this.quit();
        }

        public void set_language(string language) {
            this.language = language;
            Intl.setlocale(LocaleCategory.ALL, language);
            foreach (var page in pages) page.populate_text();
            shell.populate_text();
        }

        private Page[] make_pages() {
            Page[] pages = {};

            if (mode == INSTALLER_GREETER) {
                pages += new LanguagePage(this);
                pages += new KbdLayoutPage(this);
                pages += new DisclaimerPage();
                pages += new TryOrInstallPage(this);
            }
            if (mode == INSTALLER) {
                pages += new InstallerWelcomePage();
                pages += new DisclaimerPage();
            }
            if (mode == INSTALLER_GREETER || mode == INSTALLER) {
                //pages += new HardwareReqsPage();
                pages += new DriveSelectPage(this);
                pages += new InstallProgressPage(this);
            } else { // mode == INITIAL_SETUP
                pages += new LanguagePage(this);
                pages += new KbdLayoutPage(this);
                pages += new DisclaimerPage();
                pages += new HostnamePage();
                pages += new NetworkPage(this);
                //pages += new PrivacyPage();
                pages += new TimezonePage(this);
                pages += new UserPage(this);
                pages += new PasswordPage(this);
                pages += new ThemePage();
                //pages += new OnlineAccountsPage();
                //pages += new SoftwareUpdatesPage();
            }

            pages += new DonePage(this);
            return pages;
        }
    }

    int main(string[] args) {
        // Init localization
        Intl.setlocale();
        Intl.bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
        Intl.bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
        Intl.textdomain(GETTEXT_PACKAGE);

        // Determine if we're in the liveos
        bool is_liveos;
        try {
            string cmdline;
            FileUtils.get_contents("/proc/cmdline", out cmdline);
            is_liveos = Regex.match_simple("\\bcarbon\\.liveos\\b", cmdline);
        } catch (Error e) {
            warning("Couldn't read /proc/cmdline: %s", e.message);
            is_liveos = false; // Assume we're installed
        }

        // Determine if we're in the greeter
        var session_class = Environment.get_variable("XDG_SESSION_CLASS");
        var is_greeter = (session_class == "greeter");

        // Pick a mode
        Mode mode;
        switch (Environment.get_variable("SETUP_MODE")) {
                case "initial-setup":
                    mode = Mode.INITIAL_SETUP;
                    break;
                case "installer-greeter":
                    mode = Mode.INSTALLER_GREETER;
                    break;
                case "installer":
                    mode = Mode.INSTALLER;
                    break;
                case null: // None specified. Auto-select
                    if (is_liveos)
                        mode = is_greeter ? Mode.INSTALLER_GREETER : Mode.INSTALLER;
                    else if (is_greeter)
                        mode = Mode.INITIAL_SETUP;
                    else
                        error("No mode appropriate for environment");
                    break;
                default:
                    error("Unknown value for SETUP_MODE");
        }

        // Create the driver
        var driver = new Driver(mode);
        return driver.run(args);
    }
}

