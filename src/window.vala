[GtkTemplate(ui="/sh/carbon/setup/ui/window.ui")]
class Setup.Window : Adw.Window {
    private bool is_installer;
    [GtkChild] unowned Gtk.HeaderBar headerbar;
    [GtkChild] unowned Adw.Carousel carousel;
    [GtkChild] unowned Gtk.Stack continue_btn;
    [GtkChild] unowned Gtk.Stack back_btn;

    [GtkChild] unowned Gtk.Button next_btn_skip;
    [GtkChild] unowned Gtk.Button next_btn_next;
    [GtkChild] unowned Gtk.Button back_btn_back;

    private Page curr_page = null;
    private uint curr_page_pos = uint.MAX;
    private ulong ready_changed_id = 0;

    public Window(bool is_installer) {
        this.is_installer = is_installer;
    }

    public void add_page(Page page) {
        page.vexpand = page.hexpand = true; // Ensure the page fills the screen
        page.can_focus = false;
        page.populate_text();
        page.next.connect(() => on_next_clicked());
        page.quit.connect(() => on_quit_clicked());
        carousel.append(page);
        on_position_changed(); // Sync up
    }

    public void populate_text() {
        next_btn_skip.label = _("_Skip");
        next_btn_next.label = _("_Next");
        back_btn_back.label = _("_Back");
    }

    [GtkCallback]
    private void on_position_changed() {
        var new_curr_page_pos = (int) Math.round(carousel.position);
        if (curr_page_pos != new_curr_page_pos) {
            // De-init the old current page
            if (curr_page != null) {
                curr_page.disconnect(ready_changed_id);
                curr_page.can_focus = false;
                this.set_focus(null); // Defocus the old page
            }

            // Update the current page
            curr_page_pos = new_curr_page_pos;
            curr_page = carousel.get_nth_page(curr_page_pos) as Page;

            // Init the new current page
            curr_page.enter();
            curr_page.can_focus = true;
            curr_page.grab_focus();

            // Setup the next button
            ready_changed_id = curr_page.notify["ready"].connect(() => {
                update_continue_btn();
            });
        }

        // Always update the continue button
        update_continue_btn();
    }

    private void update_continue_btn() {
        headerbar.show_title_buttons = is_installer;
        if (curr_page.hide_next || curr_page_pos + 1 == carousel.n_pages) {
            continue_btn.visible_child_name = "empty";
            headerbar.show_title_buttons = false;
        } else if (curr_page.skippable && !curr_page.ready)
            continue_btn.visible_child_name = "skip";
        else
            continue_btn.visible_child_name = "next";
        continue_btn.sensitive = curr_page.skippable || curr_page.ready;
        back_btn.visible_child_name =
            (curr_page.hide_back || curr_page_pos == 0) ? "empty" : "back";
    }

    [GtkCallback]
    private void on_skip_clicked() {
        var target = carousel.get_nth_page(curr_page_pos + 1);
        if (Math.floor(carousel.position) == carousel.position)
            carousel.scroll_to(target, true);
    }

    [GtkCallback]
    private void on_next_clicked() {
        if (curr_page.apply())
            on_skip_clicked();
    }

    [GtkCallback]
    private void on_back_clicked() {
        var target = carousel.get_nth_page(curr_page_pos - 1);
        if (Math.floor(carousel.position) == carousel.position)
            carousel.scroll_to(target, true);
    }

    private void on_quit_clicked() {
        if (curr_page.apply())
            this.destroy();
    }
}
