class Setup.Greeter : Object {
    private Gdm.Greeter? greeter;
    private Gdm.UserVerifier? verifier;
    private string? password = null;
    private string? open_service = null;
    private SourceFunc done;

    construct {
        // Connect to the greeter
        if (!Driver.mock) {
            var client = new Gdm.Client();
            try {
                greeter = client.get_greeter_sync();
                verifier = client.get_user_verifier_sync();

                verifier.info.connect((service, msg) => {
                    message("GDM INFO: [%s] %s", service, msg);
                });
                verifier.problem.connect((service, msg) => {
                    warning("GDM PROBLEM: [%s] %s", service, msg);
                });
                verifier.info_query.connect((service, question) => {
                    warning("GDM QUESTION: [%s] %s", service, question);
                });
                verifier.secret_info_query.connect(on_secret_query);
                verifier.verification_failed.connect(() => on_auth_complete(null));
                greeter.session_opened.connect(on_auth_complete);
            } catch (Error e) {
                critical("Failed to connect to GDM: %s", e.message);
                greeter = null;
                verifier = null;
            }
        } else {
            print("Greeter: connect\n");
            greeter = null;
            verifier = null;
        }
    }

    public async void auth(string username, string? pass) {
        if (verifier == null) { // No-op impl
            print("Greeter: auth '%s', password '%s'\n", username, pass);
            return;
        }

        this.password = pass;
        done = auth.callback;
        try {
            yield verifier.call_begin_verification_for_user("gdm-password",
                                                            username, null);
            yield; // Wait for done signal
        } catch (Error e) {
            critical("Failed to start auth: %s", e.message);
        }
        done = null;
    }

    public void start_session(bool live) {
        if (greeter == null) { // No-op impl
            print("Greeter: start_session\n");
            return;
        }

        if (open_service == null) error("No authenticated session!");

        try {
            // TODO: Handle live session
            greeter.call_start_session_when_ready_sync(open_service, true, null);
        } catch (Error e) {
            error("Failed to start session: %s", e.message);
        }
    }

    private void on_secret_query(string service, string question) {
        if (password != null) {
            verifier.call_answer_query.begin(service, password, null);
            password = null;
        } else {
            warning("GDM SECRET: [%s] %s", service, question);
        }
    }

    private void on_auth_complete(string? service) {
        if (service == null) error("Failed to authenticate");
        open_service = service;
        done();
    }
}
