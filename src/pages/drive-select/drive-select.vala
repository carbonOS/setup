[GtkTemplate(ui="/sh/carbon/setup/ui/drive-select.ui")]
class Setup.DriveSelectPage : Adw.Bin, Setup.Page {
    private bool _ready = false;
    public bool ready { get { return _ready; } }
    public bool skippable { get { return false; } }

    private Driver driver;
    private ListStore model;
    private UDisks.Drive? target;
    private ulong udisks_changed_cb = 0;

    [GtkChild] unowned Gtk.ListBox box;
    [GtkChild] unowned Gtk.MessageDialog confirm_dialog;

    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Gtk.Label subtitle;
    [GtkChild] unowned Gtk.Label no_drives_msg;
    [GtkChild] unowned Gtk.Button confirm_btn_cancel;
    [GtkChild] unowned Gtk.Button confirm_btn_ok;

    public DriveSelectPage(Driver driver) {
        this.driver = driver;
        model = new ListStore(typeof(UDisks.Drive));
        target = null;
        confirm_dialog.transient_for = driver.shell;

        box.bind_model(model, it => create_disk_row(it as UDisks.Drive));
    }

    void populate_text() {
        title.label = _("Select a Drive");
        subtitle.label = _("All of the data will be erased from the drive you select. Make sure you have any important data backed up.");
        // TRANSLATORS: This line tells the user no drives were detected. The
        // "found" language here doesn't indicate a search
        no_drives_msg.label = _("No Drives Found");
        confirm_dialog.text = _("Are you sure?");
        // TRANSLATORS: Confirm that the user is ok with the drive being wiped
        // and start the installation of the OS
        confirm_btn_ok.label = _("Co_ntinue");
        confirm_btn_cancel.label = _("_Cancel");

        // If we've already entered, then changing the language might invalidate
        // the size of the disk string (commas, units, etc). Reload it
        if (udisks_changed_cb != 0) repopulate();
    }

    Adw.ActionRow create_disk_row(UDisks.Drive drive) {
        var row = new Adw.ActionRow();

        row.title = drive.get_data("pretty-name");
        row.subtitle = format_size(drive.size);
        if (drive.removable)
            row.icon_name = "usb-stick-symbolic";
        else if (drive.rotation_rate == 0)
            row.icon_name = "ssd-symbolic";
        else
            row.icon_name = "hdd-symbolic";

        row.set_data<UDisks.Drive>("drive", drive);
        row.activatable = true;
        row.activated.connect(() => next());
        return row;
    }

    void enter() {
        if (udisks_changed_cb == 0) { // Only run on the first time we enter
            repopulate(); // Initial load
            udisks_changed_cb = driver.udisks.changed.connect(() => repopulate());
        }
    }

    private UDisks.Drive? find_root_device(List<DBusObject> objects) {
        foreach (var g_obj in objects) {
            var obj = g_obj as UDisks.Object;
            if (obj.filesystem == null || obj.block == null) continue;
            if (strv_contains(obj.filesystem.mount_points, "/sysroot"))
                return driver.udisks.get_drive_for_block(obj.block);
        }
        return null; // Didn't find anything...
    }

    private void repopulate() {
        // Reset selection
        driver.target_drive = null;
        target = null;

        // Empty the list
        model.remove_all();

        // Populate the list
        var objects = driver.udisks.object_manager.get_objects();
        var root_dev = find_root_device(objects);
        foreach (var g_obj in objects) {
            var obj = g_obj as UDisks.Object;
            var drive = obj.drive;
            if (drive == null) continue;
            if (drive.optical) continue; // Never install onto optical media
            if (drive == root_dev) continue; // Never install over our backing OS
            if (drive.size == 0) continue; // Never install to an empty SD slot

            // Check for an associated physical block device
            var block = driver.udisks.get_block_for_drive(drive, true);
            if (block == null) continue;
            drive.set_data("block", block);

            // Generate the pretty name for this drive
            string[] title_parts = {};
            if (drive.vendor != "") title_parts += drive.vendor;
            if (drive.model != "") title_parts += drive.model;
            var pretty_name = string.joinv(" ", title_parts);
            drive.set_data("pretty-name", pretty_name);

            // Add into the list, sorted by sort_key
            model.insert_sorted(drive, (a, b) => {
                var keyA = ((UDisks.Drive) a).sort_key;
                var keyB = ((UDisks.Drive) b).sort_key;
                return strcmp(keyA, keyB);
            });
        }

        // Show the no drives error if necessary
        no_drives_msg.visible = (model.get_n_items() == 0);

        // Ensure that we don't have anything selected after a hotplug
        Idle.add(() => {
            box.select_row(null);
            return Source.REMOVE;
        });
    }

    [GtkCallback]
    void on_selected(Gtk.ListBox _box, Gtk.ListBoxRow? row) {
        _ready = (row != null);
        if (_ready) target = row.get_data<UDisks.Drive>("drive");
        this.notify_property("ready");
    }

    bool apply() {
        // Ask the user to confirm if they want to wipe the drive
        var confirmed_wipe = false;
        var loop = new MainLoop();
        confirm_dialog.secondary_text =
            // TRANSLATORS: Format is the name of a disk drive, for example:
            // "WD Blue" or "Samsung 840 EVO"
            _("All of the apps and data on this <b>%s</b> will be erased!").
            printf(target.get_data("pretty-name"));
        confirm_dialog.response.connect(code => {
            confirmed_wipe = (code == Gtk.ResponseType.YES);
            confirm_dialog.hide();
            loop.quit();
        });
        confirm_dialog.show();
        loop.run();
        if (!confirmed_wipe) return false;

        // Tell the driver about the selected drive
        driver.target_drive = target;
        return true; // We're good to proceed
    }
}
