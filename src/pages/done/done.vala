[GtkTemplate(ui="/sh/carbon/setup/ui/done.ui")]
class Setup.DonePage : Adw.Bin, Setup.Page {
    private bool _ready = false;
    public bool ready { get { return _ready; } }
    public bool hide_back { get { return true; } }

    private Driver driver;
    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Gtk.Label subtitle;
    [GtkChild] unowned Gtk.Button done;

    public DonePage(Driver driver) {
        this.driver = driver;
    }

    void populate_text() {
        var is_installer = (driver.mode != INITIAL_SETUP);
        title.label = is_installer ?
            _("Almost Done…") :
            _("You're all set up!");
        subtitle.label = is_installer ?
            _("Your computer needs to restart to finish the setup") :
            _("Your computer is now ready to be used");
        done.label = is_installer ?
            _("_Reboot") :
            // TRANSLATORS: This button completes setup and takes the user to
            // their new desktop
            _("_Done");
    }

    void enter() {
        if (driver.mode == Mode.INITIAL_SETUP) {
            driver.finish_setup_new_user.begin((obj, res) => {
                driver.finish_setup_new_user.end(res);
                notify_ready();
            });
        } else {
            notify_ready();
            if (!driver.shell.is_active) {
                var notif = new Notification(title.label);
                notif.set_body(subtitle.label);
                notif.add_button(_("Reboot"), "app.reboot");
                notif.set_default_action("app.focus");
                driver.send_notification(null, notif);
            }
        }
    }

    [GtkCallback]
    void on_quit() {
        this.quit();
    }

    private void notify_ready() {
        _ready = true;
        this.notify_property("ready");
    }

    bool apply() {
        if (driver.mode != Mode.INITIAL_SETUP)
            driver.reboot.begin();
        else
            driver.start_user_session();
        return true;
    }
}
