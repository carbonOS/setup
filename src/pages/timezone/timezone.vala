using GWeather;

[GtkTemplate(ui="/sh/carbon/setup/ui/timezone.ui")]
class Setup.TimezonePage : Adw.Bin, Setup.Page {
    public bool ready { get { return selected_tzone != null; } }
    private bool _hide_next = true;
    public bool hide_next { get { return _hide_next; } }

    private Driver driver;
    private ListStore search_store;
    private SearchResult? selected_tzone = null;
    private uint auto_clock_timeout = 0;
    private bool user_disabled_auto = false;

    [GtkChild] unowned Gtk.Stack switcher;
    [GtkChild] unowned Gtk.Box auto_root;
    [GtkChild] unowned Gtk.Label time_prompt;
    [GtkChild] unowned Gtk.Label current_time;
    [GtkChild] unowned Gtk.Button incorrect_btn;
    [GtkChild] unowned Gtk.Button correct_btn;
    [GtkChild] unowned Gtk.Box search_root;
    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Gtk.Label subtitle;
    [GtkChild] unowned Gtk.SearchEntry searchbox;
    [GtkChild] unowned Gtk.ListBox search_results;

    // For performance reasons, we don't want to recurse too deep
    private const int SEARCH_RESULT_COUNT_LIMIT = 20;

    class SearchResult : Object {
        public Location loc;
        public TimeZone zone;

        // We sort name matches before everything else
        public bool name_match;

        public SearchResult(Location loc, TimeZone zone) {
            this.loc = loc;
            this.zone = zone;
            this.name_match = false;
        }
    }

    public TimezonePage(Driver driver) {
        this.driver = driver;

        // Set up the search results
        this.search_store = new ListStore(typeof(SearchResult));
        search_results.bind_model(search_store, it => {
            return create_search_result_row(it as SearchResult);
        });

        // TODO: Remove this
        // For carbonOS 2021.1, I'm going to disable automatic timezone
        // functionality. I think I need to redesign it (see #4 for details).
        // So, for now, let's do this immediately @ construction time:
        switch_to_manual_selection();
    }

    void populate_text() {
        title.label = _("Timezone");
        subtitle.label = _("Search for your city to set the clock");
        time_prompt.label = _("Is this the correct time?");
        incorrect_btn.label = _("No");
        correct_btn.label = _("Yes");
    }

    void enter() {
        // TODO: Immediately switch to the manual tz selection page
        // if location services are disabled/offline. Use:
        // switch_to_manual_selection()

        // Reset the search
        if (selected_tzone == null)
            searchbox.text = "";

        // Start ticking the auto clock
        if (switcher.visible_child == auto_root) {
            on_auto_clock_tick();
            if (auto_clock_timeout == 0)
                auto_clock_timeout = Timeout.add(500, on_auto_clock_tick);
        }
    }

    bool apply() {
        // We're going to let g-s-d-datetime manage timezone in the auto case
        if (switcher.visible_child == auto_root) return true;

        var tzid = selected_tzone.zone.get_identifier();

        if (Driver.mock) {
            print("apply_timezone: %s\n", tzid);
            return true;
        }

        // Set the timezone
        try {
            var conn = Bus.get_sync(BusType.SYSTEM);
            conn.call_sync("org.freedesktop.timedate1",
                "/org/freedesktop/timedate1",
                "org.freedesktop.timedate1",
                "SetTimezone",
                new Variant("(sb)", tzid, false),
                null, DBusCallFlags.NONE, -1);
        } catch (Error e) {
            critical("Failed to set timezone: %s", e.message);
            return true; // Should be allowed to continue anyway
        }

        // If the user manually declined the automatic timezone, disable
        // the feature. Otherwise keep it enabled (just in case the user
        // later enables location services or connects to the internet).
        var settings = new Settings("org.gnome.desktop.datetime");
        settings.set_boolean("automatic-timezone", !user_disabled_auto);

        // Also, set the clock-format settings based on the selected locale
        // This is here just b/c it seems like the most reasonable place
        // to put it. TODO: This needs to be done even if automatic tz is on
        settings = new Settings("org.gnome.desktop.interface");
        var t_fmt = Posix.nl_langinfo(Posix.NLItem.T_FMT);
        var is_12h = t_fmt.contains("%r") || t_fmt.contains("%l") ||
            t_fmt.contains("%I");
        settings.set_string("clock-format", is_12h ? "12h" : "24h");

        return true;
    }

    private bool on_auto_clock_tick() {
        current_time.label = new DateTime.now().format("%X");
        return Source.CONTINUE;
    }

    private void switch_to_manual_selection() {
        // Show the next button
        _hide_next = false;
        this.notify_property("ready"); // we can't notify hide-next directly

        // Show the search page
        switcher.visible_child = search_root;

        // Stop ticking the auto clock
        if (auto_clock_timeout != 0) {
            Source.remove(auto_clock_timeout);
            auto_clock_timeout = 0;
        }
    }

    [GtkCallback]
    private void on_incorrect_btn_clicked() {
        switch_to_manual_selection();
        user_disabled_auto = true;
    }

    [GtkCallback]
    private void on_correct_btn_clicked() {
        // Stop ticking the auto clock
        Source.remove(auto_clock_timeout);
        auto_clock_timeout = 0;

        // Apply and continue
        next();
    }

    [GtkCallback]
    private void on_search_changed() {
        search_store.remove_all();

        var query = searchbox.text.normalize().casefold();
        if (query != "") do_search(Location.get_world(), query);

        search_store.sort((a, b) => {
            var sr_a = a as SearchResult;
            var sr_b = b as SearchResult;

            // Sort name_match names first!
            if (sr_a.name_match && !sr_b.name_match) return -1;
            if (!sr_a.name_match && sr_b.name_match) return 1;

            var name_a = sr_a.loc.get_sort_name();
            var name_b = sr_b.loc.get_sort_name();
            return strcmp(name_a, name_b);
        });

        search_results.visible = (search_store.get_n_items() != 0);
    }

    [GtkCallback]
    private void on_selected_row_changed() {
        var row = search_results.get_selected_row();
        if (row == null) {
            selected_tzone = null;
            this.notify_property("ready");
        } else {
            selected_tzone = row.get_child().get_data("result");
            this.notify_property("ready");
        }
    }

    private Gtk.Widget create_search_result_row(SearchResult result) {
        var name_text = "<b>%s</b>".printf(result.loc.get_name());

        // Append timezone abbreviation/offset
        if (result.zone.get_identifier() != "UTC") { // UTC already has this
            var now = new DateTime.now(result.zone);
            var tz_name = now.format("%Z");

            // The %Z format sometimes spits out ugly offsets. Let's
            // make them nicer
            if (tz_name[0] == '+' || tz_name[0] == '-')
                // TRANSLATORS: UTC here means Coordinated Universal Time.
                // the %:z will be replaced by the offset (i.e. UTC+06:00)
                tz_name = now.format(_("UTC%:z"));

            name_text += " (%s)".printf(tz_name);
        }

        // Parent location to give user more context
        var country_name = result.loc.get_country_name();
        string? adm_region_name = null; // i.e. state, province
        if (result.loc.get_parent().get_level() == LocationLevel.ADM1)
            adm_region_name = result.loc.get_parent().get_name();
        string? region_name;
        if (adm_region_name != null && country_name != null)
            region_name = "%s, %s".printf(adm_region_name, country_name);
        else
            region_name = country_name;

        var root = new Gtk.Box(Gtk.Orientation.VERTICAL, 6);
        root.margin_top = root.margin_bottom = 10;
        root.margin_start = root.margin_end = 10;

        var name_lbl = new Gtk.Label(name_text);
        name_lbl.halign = Gtk.Align.START;
        name_lbl.use_markup = true;
        name_lbl.ellipsize = Pango.EllipsizeMode.END;
        root.append(name_lbl);

        if (region_name != null) {
            var region_lbl = new Gtk.Label(region_name);
            region_lbl.halign = Gtk.Align.START;
            region_lbl.add_css_class("dim-label");
            root.append(region_lbl);
        }

        root.set_data("result", result);
        return root;
    }

    // Written with reference to GNOME Clock's world location dialog source.
    // https://gitlab.gnome.org/GNOME/gnome-clocks/-/blob/master/src/world-location-dialog.vala
    private void do_search(GWeather.Location location, string query) {
        if (search_store.get_n_items() >= SEARCH_RESULT_COUNT_LIMIT) return;

        switch (location.get_level()) {
            case CITY:
                var zone = location.get_timezone();
                if (zone == null) return;

                var name_contains = location.get_sort_name().contains(query);

                var country_contains = false;
                string? country_name = location.get_country_name();
                if (country_name != null) {
                    country_name = country_name.normalize().casefold();
                    country_contains = country_name.contains(query);
                }

                if (name_contains || country_contains) {
                    var result = new SearchResult(location, zone);
                    result.name_match = name_contains;
                    search_store.append(result);
                }
                return; // Don't search deeper than cities
            case NAMED_TIMEZONE:
                if (location.get_sort_name().contains(query)) {
                    var zone = location.get_timezone();
                    if (zone == null) return;
                    var result = new SearchResult(location, zone);
                    search_store.append(result);
                }
                return; // Named timezones don't have children
            default:
                break;
        }

        // Recursively search all children
        Location? child = null;
        while ((child = location.next_child(child)) != null) {
            do_search(child, query);
            if (search_store.get_n_items() >= SEARCH_RESULT_COUNT_LIMIT) return;
        }
    }
}
