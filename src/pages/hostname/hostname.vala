namespace CC {
    // Helper method from gnome-control-center, to convert a pretty hostname
    // to a static hostname
    [CCode(cname="cc_util_pretty_hostname_to_static")]
    extern string pretty_to_static(string input, bool disp);
}

[GtkTemplate(ui="/sh/carbon/setup/ui/hostname.ui")]
class Setup.HostnamePage : Adw.Bin, Setup.Page {
    public bool ready { get { return entry.text != ""; } }

    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Gtk.Label subtitle;
    [GtkChild] unowned Gtk.Entry entry;

    void populate_text() {
        title.label = _("Name this Device");
        subtitle.label = _("This is used to identify this device when communicating with other devices nearby and on the network.");
    }

    [GtkCallback]
    void on_entry_change() {
        this.notify_property("ready");
    }

    [GtkCallback]
    void on_entry_activate() {
        if (ready) next();
    }

    [DBus(name="org.freedesktop.hostname1")]
    private interface Hostnamed : DBusProxy {
        public async abstract void set_pretty_hostname(string name,
            bool interactive) throws Error;

        public async abstract void set_static_hostname(string name,
            bool interactive) throws Error;
    }

    private async void set_hostname() {
        try {
            var hostnamed = yield Bus.get_proxy<Hostnamed>(BusType.SYSTEM,
                "org.freedesktop.hostname1", "/org/freedesktop/hostname1");

            yield hostnamed.set_pretty_hostname(entry.text, true);

            var static_hostname = CC.pretty_to_static(entry.text, false);
            assert_nonnull(static_hostname);
            yield hostnamed.set_static_hostname(static_hostname, true);
        } catch (Error e) {
            critical("Failed to set hostname: %s", e.message);
        }
    }

    bool apply() {
        if (Driver.mock)
            print("set_hostname: pretty='%s', static='%s'\n",
                entry.text, CC.pretty_to_static(entry.text, false));
        else
            set_hostname.begin();
        return true;
    }
}
