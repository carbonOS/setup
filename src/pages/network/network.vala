using NM;

[GtkTemplate(ui="/sh/carbon/setup/ui/network.ui")]
class Setup.NetworkPage : Adw.Bin, Setup.Page {
    private bool _ready = false;
    public bool ready { get { return _ready; } }
    public bool skippable { get { return true; } }

    private Client nm;
    private Driver driver;
    private bool connected_nm_signals = false;

    [GtkChild] unowned Gtk.Stack stack;

    [GtkChild] unowned Gtk.Box wifi_page;
    [GtkChild] unowned Gtk.Label wifi_title;
    [GtkChild] unowned Gtk.Label wifi_subtitle;
    [GtkChild] unowned Gtk.ListBox wifi_list;
    [GtkChild] unowned Gtk.Label hidden_row;

    [GtkChild] unowned Gtk.Box already_connected_page;
    [GtkChild] unowned Gtk.Image already_connected_icon;
    [GtkChild] unowned Gtk.Label already_connected_title;
    [GtkChild] unowned Gtk.Button already_connected_btn;

    [GtkChild] unowned Gtk.Box no_dev_page;
    [GtkChild] unowned Gtk.Label no_dev_title;
    [GtkChild] unowned Gtk.Label no_dev_subtitle;

    [GtkChild] unowned Gtk.Box plug_in_page;
    [GtkChild] unowned Gtk.Label plug_in_title;
    [GtkChild] unowned Gtk.Label plug_in_subtitle;

    public NetworkPage(Driver driver) {
        this.driver = driver;

        try {
            this.nm = new Client();
        } catch (Error e) {
            critical("Failed to connect to NetworkManager: %s", e.message);
            this.nm = null;
        }
    }

    void populate_text() {
        wifi_title.label = _("Connect to Wi-Fi");
        hidden_row.label = _("Hidden Network…");
        already_connected_title.label = _("You're Already Connected");
        already_connected_btn.label = _("Connect to Wi-Fi");
        no_dev_title.label = _("No Network Devices");
        no_dev_subtitle.label = _("No available network devices were found. You can continue setting up your device without an internet connection.");
        plug_in_title.label = _("Plug in your Ethernet");
        plug_in_subtitle.label = _("For a better experience, connect your device to the network via Ethernet. You can continue setting up your device without an internet connection.");
    }

    void enter() {
        rescan();
        if (!connected_nm_signals && nm != null) {
            nm.device_added.connect(it => rescan());
            nm.device_removed.connect(it => rescan());
            nm.active_connection_added.connect(it => rescan());
            nm.active_connection_removed.connect(it => rescan());
            connected_nm_signals = true;
        }
    }

    void rescan() {
        // Reset everything
        wifi_subtitle.label = "";
        wifi_subtitle.visible = true;
        already_connected_icon.icon_name = "network-wired-symbolic";
        already_connected_btn.visible = false;
        stack.transition_type = Gtk.StackTransitionType.NONE;
        set_ready(false);

        // Probe devices
        var has_eth = false;
        var eth_connected = false;
        DeviceWifi? primary_wlan = null;
        var mobile_connected = false;
        if (nm != null) {
            nm.devices.foreach(dev => {
                if (dev is DeviceWifi) {
                    if (primary_wlan == null) primary_wlan = dev as DeviceWifi;
                } else if (dev is DeviceEthernet) {
                    has_eth |= true;
                    eth_connected |= (dev.state == DeviceState.ACTIVATED);
                } else if (dev is DeviceModem) {
                    mobile_connected |= (dev.state == DeviceState.ACTIVATED);
                }
            });
        }

        // Set-up the proper view to show
        if (eth_connected) {
            stack.visible_child = already_connected_page;
            already_connected_btn.visible = (primary_wlan != null);
            // TRANSLATORS: This is shown on the wifi selection page if the
            // device is already connected to ethernet
            wifi_subtitle.label = _("You're already connected via Ethernet.");
            set_ready(true);
        } else if (mobile_connected) {
            if (primary_wlan != null) {
                stack.visible_child = wifi_page;
                // TRANSLATORS: This is shown on the wifi selection page if the
                // device is already connected to a mobile carrier network
                wifi_subtitle.label = _("You're already connected to a mobile network. Connect to Wi-Fi for a better experience.");
            } else {
                stack.visible_child = already_connected_page;
                already_connected_icon.icon_name = "network-cellular-symbolic";
                set_ready(true);
                return;
            }
        } else if (has_eth) {
            if (primary_wlan != null) {
                stack.visible_child = wifi_page;
                // TRANSLATORS: This is shown on the wifi selection page if the
                // device has an ethernet port but is not connected to it
                wifi_subtitle.label = _("You can also connect via Ethernet.");
            } else {
                stack.visible_child = plug_in_page;
                return;
            }
        } else {
            wifi_subtitle.visible = false;
            if (primary_wlan != null)
                stack.visible_child = wifi_page;
            else {
                set_ready(true);
                stack.visible_child = no_dev_page;
            }
        }

        // Populate the wifi list
        if (primary_wlan != null) {
            var model = new WifiModel(primary_wlan, ready ? null : this);
            wifi_list.bind_model(model, it => create_row(it as WifiNetwork));
        }
    }

    private Gtk.Widget create_row(WifiNetwork network) {
        if (network.is_hidden) {
            return hidden_row;
        } else {
            return new WifiRow(network);
        }
    }

    [GtkCallback]
    private void on_wifi_anyway_btn_clicked() {
        stack.transition_type = Gtk.StackTransitionType.CROSSFADE;
        stack.visible_child = wifi_page;
    }

    private void open_hidden_network_dialog() {
        Driver driver = Application.get_default() as Driver;
        var dialog = new Gtk.MessageDialog(driver.shell, Gtk.DialogFlags.MODAL,
            Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, null);
        dialog.text = _("Feature isn't implemented");
        dialog.secondary_text = _("Setup cannot connect to hidden networks yet");
        dialog.response.connect(() => dialog.destroy());
        dialog.show();
    }

    private void activate_wifi_network_connection(WifiNetwork network) {
        var connections = nm.get_connections();
        var dev_connections = network.device.filter_connections(connections);
        var ap_connections = network.ap.filter_connections(dev_connections);

        if (ap_connections.length != 0) {
            nm.activate_connection_async.begin(ap_connections[0], network.device,
                null, null, (obj, res) => {
                    try {
                        nm.activate_connection_async.end(res);
                    } catch (Error e) {
                        warning("Failed to activate network: %s", e.message);
                    }
                });
        } else {
            nm.add_and_activate_connection_async.begin(null, network.device,
                network.ap.path, null, (obj, res) => {
                    try {
                        nm.add_and_activate_connection_async.end(res);
                    } catch (Error e) {
                        warning("Failed to add/activate network: %s", e.message);
                    }
                });
        }
        network.activating();
    }

    [GtkCallback]
    private void on_wifi_row_selected(Gtk.ListBoxRow row) {
        if (row.child == hidden_row) {
            open_hidden_network_dialog();
            return;
        }

        var network = ((WifiRow) row).network;
        activate_wifi_network_connection(network);
    }

    public void set_ready(bool is_ready) {
        _ready = is_ready;
        notify_property("ready");
    }
}

class Setup.WifiRow : Gtk.ListBoxRow {
    public WifiNetwork network;

    public WifiRow(WifiNetwork network) {
        this.network = network;

        var box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 6);
        box.margin_start = box.margin_end = 10;
        box.margin_top = box.margin_bottom = 10;
        this.set_child(box);

        var strength = new Gtk.Image.from_icon_name(network.icon_name);
        box.append(strength);

        var label = new Gtk.Label(network.disp_ssid);
        box.append(label);

        var image = new Gtk.Image.from_icon_name("object-select-symbolic");
        image.opacity = (network.active) ? 1 : 0;
        image.add_css_class("accent");
        box.append(image);

        var spinner = new Gtk.Spinner();
        spinner.start();
        spinner.visible = false;
        box.append(spinner);

        network.activating.connect(() => {
            image.visible = false;
            spinner.visible = true;
        });
    }
}

class Setup.WifiNetwork : GLib.Object {
    public string disp_ssid;
    public string icon_name;
    public bool active;

    public DeviceWifi device;
    public AccessPoint ap;

    public WifiNetwork(DeviceWifi device, AccessPoint ap) {
        this.device = device;
        this.ap = ap;

        this.disp_ssid = Utils.ssid_to_utf8(ap.ssid.get_data());
        this.icon_name = strength_to_icon(ap.strength);
        this.active = (device.active_access_point == ap);
    }

    // Matches gnome-control-center
    private static string strength_to_icon(uint strength) {
        if (strength >= 80)
            return "network-wireless-signal-excellent-symbolic";
        if (strength >= 50)
            return "network-wireless-signal-good-symbolic";
        if (strength >= 40)
            return "network-wireless-signal-ok-symbolic";
        if (strength >= 20)
            return "network-wireless-signal-weak-symbolic";
        return "network-wireless-signal-none-symbolic";
    }

    public signal void activating();

    public bool is_hidden;
    public WifiNetwork.hidden_btn() {
        is_hidden = true;
    }

    public static int compare(GLib.Object a, GLib.Object b) {
        var wa = a as WifiNetwork, wb = b as WifiNetwork;
        if (wa.active && !wb.active) return -1;
        if (wb.active && !wa.active) return 1;
        return wb.ap.strength - wa.ap.strength;
    }
}

class Setup.WifiModel : ListModel, GLib.Object {
    private DeviceWifi device;
    private ListStore base_model;
    private NetworkPage? page;

    private Cancellable cancellable;
    private uint refresh_timer = 0;
    private ulong notify_signal = 0;

    public WifiModel(DeviceWifi device, NetworkPage? page) {
        this.device = device;
        this.base_model = new ListStore(typeof(WifiNetwork));
        this.page = page;

        // Delegate the items_changed signal to the base model
        base_model.items_changed.connect((pos, rm, add) => {
            this.items_changed(pos, rm, add);
        });

        cancellable = new Cancellable();
        refresh_timer = Timeout.add(10000, on_scan_timeout);
        notify_signal = device.notify.connect(() => populate());
        populate();
    }

    ~WifiModel() {
        ((GLib.Object) device).disconnect(notify_signal);
        Source.remove(refresh_timer);
        cancellable.cancel();
    }

    Type get_item_type() {
        return typeof(WifiNetwork);
    }

    uint get_n_items() {
        return base_model.get_n_items() + 1;
    }

    GLib.Object? get_item(uint pos) {
        if (pos < base_model.get_n_items())
            return base_model.get_item(pos);
        return new WifiNetwork.hidden_btn();
    }

    private bool on_scan_timeout() {
        device.request_scan_async.begin(cancellable, (obj, res) => {
            try {
                device.request_scan_async.end(res);
                populate();
            } catch (Error e) {
                warning("Failed to scan wifi: %s\n", e.message);
            }
        });
        return Source.CONTINUE;
    }

    private void populate() {
        base_model.remove_all();
        var has_active = false;
        device.access_points.foreach(ap => {
            var network = new WifiNetwork(device, ap);
            has_active |= network.active;
            base_model.insert_sorted(network, WifiNetwork.compare);
        });
        if (page != null) page.set_ready(has_active);
    }
}
