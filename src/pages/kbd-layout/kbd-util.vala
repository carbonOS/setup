namespace Setup.Kbd {
    private static Gnome.XkbInfo xkb_info;
    private static HashTable<string, IBus.EngineDesc> ibus_engines;

    public void init() {
        xkb_info = new Gnome.XkbInfo();

        // Load ibus engines
        IBus.init();
        ibus_engines = new HashTable<string, IBus.EngineDesc>(str_hash, str_equal);
        var ibus = new IBus.Bus.@async();
        if (ibus.is_connected())
            populate_ibus_engines(ibus);
        else {
            ulong hdl = 0;
            hdl = ibus.connected.connect(() => {
                populate_ibus_engines(ibus);
                ibus.disconnect(hdl);
            });
        }
        maybe_start_ibus();
    }

    private void populate_ibus_engines(IBus.Bus ibus) {
        foreach (var engine in ibus.list_engines()) {
            // Skip xkb engines, since we handle xkb on our own
            if (engine.name.has_prefix("xkb:")) continue;
            ibus_engines.replace(engine.name, engine);
        }
    }

    private void maybe_start_ibus() {
        // This touches ibus's well-known dbus name to start it
        var watch = Bus.watch_name(BusType.SESSION, IBus.SERVICE_IBUS,
                                   BusNameWatcherFlags.AUTO_START);
        Bus.unwatch_name(watch);
    }

    public string make_ibus_disp_name(IBus.EngineDesc engine) {
        var name = engine.longname;
        if (engine.textdomain != "" && name != "")
            name = dgettext(engine.textdomain, name);
        var language = IBus.get_language_name(engine.language);
        return "%s (%s)".printf(language, name);
    }

    // Tell localed about the selected keyboard layouts
    [DBus(name="org.freedesktop.locale1")]
    private interface Localed : DBusProxy {
        public async abstract void set_x11_keyboard(string layout, string model,
            string variant, string options, bool convert, bool interactive)
            throws Error;
    }
    public async void set_localed_kbd(string layout, string variant) {
        if (Driver.mock) {
            print("set_localed_kbd: layout=%s, variant=%s\n", layout, variant);
            return;
        }

        try {
            var proxy = yield Bus.get_proxy<Localed>(BusType.SYSTEM,
                "org.freedesktop.locale1", "/org/freedesktop/locale1");
            yield proxy.set_x11_keyboard(layout, "", variant, "", true, true);
        } catch (Error e) {
            critical("Failed to set logind locale: %s", e.message);
        }
    }
}
