using Gnome.Languages;

namespace Setup.Kbd {
    enum Kind {
        XKB,
        IBUS;

        public static Kind from_string(string kind) {
            switch (kind) {
                case "xkb": return Kind.XKB;
                case "ibus": return Kind.IBUS;
                default: assert_not_reached();
            }
        }

        public string to_string() {
            switch (this) {
                case Kind.XKB: return "xkb";
                case Kind.IBUS: return "ibus";
                default: assert_not_reached();
            }
        }
    }

    // One keyboard layout in KbdModel
    private class KbdLayout : Object {
        public Kind kind;
        public string id;
        public string? display_name = null;

        // xkb settings
        public string? xkb_layout = null;
        public string? xkb_variant = null;

        // Filter & sort
        public bool is_primary = false;
        public bool is_locale = false;

        public KbdLayout(Kind kind, string id) {
            this.kind = kind;
            this.id = id;

            switch (kind) {
                case Kind.XKB:
                    xkb_info.get_layout_info(id, out this.display_name,
                                             null, out this.xkb_layout,
                                             out this.xkb_variant);
                    break;
                case Kind.IBUS:
                    this.display_name = make_ibus_disp_name(ibus_engines[id]);
                    break;
                default:
                    assert_not_reached();
            }
        }

        public bool is_more = false;
        public KbdLayout.more_btn() {
           is_more = true;
        }
    }

    // A ListModel of all the keyboard layouts on the system (with a more item)
    private class KbdModel : ListModel, Object {
        private ListStore base_model;
        private Gtk.Filter filter;
        private Gtk.FilterListModel filter_model;
        private GenericSet<string> seen_inputs;
        private string filter_text = "";

        construct {
            // Create the base model
            base_model = new ListStore(typeof(KbdLayout));

            // Setup the filter
            filter = new Gtk.CustomFilter(it => filter_func(it as KbdLayout));
            filter_model = new Gtk.FilterListModel(base_model, filter);

            // Delegate the items_changed signal to the filter_model
            filter_model.items_changed.connect((pos, rm, add) => {
                this.items_changed(pos, rm, add);
            });
        }

        private bool filter_func(KbdLayout item) {
            // Filter based on search box text
            if (item.display_name != null &&
                filter_text.match_string(item.display_name, true))
                return true;
            return false;
        }

        private int sort_func(KbdLayout a, KbdLayout b) {
            // The primary layout always goes first
            if (a.is_primary && !b.is_primary) return -1;
            if (!a.is_primary && b.is_primary) return 1;

            // Locale layouts always go next
            if (a.is_locale && !b.is_locale) return -1;
            if (!a.is_locale && b.is_locale) return 1;

            // The rest of the layouts are sorted alphabetically
            if (a.display_name != null && b.display_name != null) {
                var cmp = strcmp(a.display_name, b.display_name);
                if (cmp != 0) return cmp;
            }

            // Fallback to sort by id
            return strcmp(a.id, b.id);
        }

        Type get_item_type() {
            return typeof(KbdLayout);
        }

        uint get_n_items() {
            return filter_model.get_n_items();
        }

        Object? get_item(uint pos) {
            if (pos < filter_model.get_n_items())
                return filter_model.get_item(pos);
            assert_not_reached();
        }

        private void insert(Kind kind, string id, bool is_locale) {
            var key = "%s::%s".printf(kind.to_string(), id);

            if (key in seen_inputs) return;
            seen_inputs.add(key);

            var layout = new KbdLayout(kind, id);
            layout.is_primary = false;
            layout.is_locale = is_locale;
            base_model.insert_sorted(layout, (a, b) => {
                return sort_func(a as KbdLayout, b as KbdLayout);
            });
        }

        public void repopulate(string? locale) {
            base_model.remove_all();
            seen_inputs = new GenericSet<string>(str_hash, str_equal);
            if (locale == null) return;

            // Layouts to ignore
            seen_inputs.add("xkb::custom");

            string lang; // Name of the language (i.e. en)
            string? country; // Name of the region (i.e. US)
            parse_locale(locale, out lang, out country, null, null);

            // Add default input source for locale
            string? def_kind = null, def_id = null;
            get_input_source_from_locale(locale, out def_kind, out def_id);
            if (def_kind != null && def_id != null) {
                seen_inputs.add("%s::%s".printf(def_kind, def_id));
                var layout = new KbdLayout(Kind.from_string(def_kind), def_id);
                layout.is_primary = true;
                base_model.append(layout);
            }

            // Add layouts for language
            var layouts = xkb_info.get_layouts_for_language(lang);
            foreach (var layout in layouts) insert(Kind.XKB, layout, true);

            // Add layouts for country
            if (country != null) {
                layouts = xkb_info.get_layouts_for_country(country);
                foreach (var layout in layouts) insert(Kind.XKB, layout, true);
            }

            // Add the rest of the xkb layouts
            layouts = xkb_info.get_all_layouts();
            foreach (var layout in layouts) insert(Kind.XKB, layout, false);

            // Add the ibus layouts
            ibus_engines.for_each((id, engine) => {
                insert(Kind.IBUS, id, engine.language == lang);
            });
        }

        public void update_filter(string text) {
            this.filter_text = text;
            filter.changed(Gtk.FilterChange.DIFFERENT);
        }

        public KbdLayout? find(Kind kind, string id) {
            for (uint i = 0; i < base_model.get_n_items(); i++) {
                var item = base_model.get_item(i) as KbdLayout;
                if (item.kind == kind && item.id == id) return item;
            }
            return null;
        }
    }

    // One keyboard input in the keyboard picker widget
    [GtkTemplate(ui="/sh/carbon/setup/ui/kbd-layout-row.ui")]
    class LayoutRow : Gtk.ListBoxRow {
        private Picker picker;
        public KbdLayout layout;

        [GtkChild] unowned Gtk.Label label;
        [GtkChild] unowned Gtk.Image checkmark;
        [GtkChild] unowned Gtk.Button preview_btn;

        public LayoutRow(Picker picker, KbdLayout layout) {
            this.picker = picker;
            this.layout = layout;
            label.label = layout.display_name;
            preview_btn.visible = (layout.kind == Kind.XKB);
            picker.notify["selected"].connect(update_check);
            update_check();
        }

        private void update_check() {
            checkmark.opacity = (picker.selected == layout) ? 1 : 0;
        }

        [GtkCallback]
        void open_preview() {
            string cmdline;
            if (layout.xkb_variant != null)
                cmdline = "gkbd-keyboard-display -l \"%s\t%s\""
                    .printf(layout.xkb_layout, layout.xkb_variant);
            else
                cmdline = "gkbd-keyboard-display -l \"%s\""
                    .printf(layout.xkb_layout);
            try {
                Process.spawn_command_line_async(cmdline);
            } catch (Error e) {
                warning("Failed to spawn process: %s", e.message);
            }
        }
    }

    // Keyboard picker widget
    [GtkTemplate(ui="/sh/carbon/setup/ui/kbd-layout-picker.ui")]
    class Picker : Gtk.Box {
        private KbdModel model;
        [GtkChild] unowned Gtk.Entry search_box;
        [GtkChild] unowned Gtk.ListBox list_box;
        [GtkChild] unowned Gtk.Label empty_state;
        [GtkChild] unowned Gtk.Stack placeholder;

        public KbdLayout? selected { get; set; default = null; }

        construct {
            model = new KbdModel();
            model.items_changed.connect(on_model_changed);
            on_model_changed();
            list_box.bind_model(model, it => new LayoutRow(this, it as KbdLayout));
        }

        public void update_locale(string? locale) {
            // TRANSLATORS: Found here means that the user searched for a
            // keyboard layout that isn't in the list.
            empty_state.label = _("No Keyboard Layouts Found");
            search_box.placeholder_text = _("Search");

            // De-select
            selected = null;

            // Change sort order & etc. Pre-select the right model
            Idle.add(() => {
                model.repopulate(locale);
                if (locale != null) {
                    string kind, id;
                    get_input_source_from_locale(locale, out kind, out id);
                    selected = model.find(Kind.from_string(kind), id);
                }
                return Source.REMOVE;
            });
        }

        private void on_model_changed() {
            if (model.get_n_items() == 0)
                placeholder.visible_child = empty_state;
            else
                placeholder.visible_child = list_box;
        }

        [GtkCallback]
        private void on_search_changed() {
            model.update_filter(search_box.text);
        }

        [GtkCallback]
        private void on_activated(Gtk.ListBoxRow row) {
            var layout = ((LayoutRow) row).layout;
            if (layout != this.selected)
                this.selected = layout;
            else
                confirm();
        }

        public signal void confirm();
    }
}
