using Setup.Kbd;

[GtkTemplate(ui="/sh/carbon/setup/ui/kbd-layout.ui")]
class Setup.KbdLayoutPage : Adw.Bin, Setup.Page {
    private bool _ready = false;
    public bool ready { get { return _ready; } }
    public bool skippable { get { return false; } }

    private Driver driver;
    private Settings settings;
    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Gtk.Entry test_entry;
    [GtkChild] unowned Picker picker;

    public KbdLayoutPage(Driver driver) {
        this.driver = driver;
        this.settings = new Settings("org.gnome.desktop.input-sources");
    }

    void populate_text() {
        title.label = _("Keyboard");
        test_entry.placeholder_text = _("Test your settings here");
        picker.update_locale(driver.language);
    }

    private bool set_layout(Kind kind, string id) {
        var builder = new VariantBuilder(new VariantType("a(ss)"));
        builder.add("(ss)", kind.to_string(), id);
        var data = builder.end();

        var changed = !settings.get_value("sources").equal(data);
        this.settings.set_value("sources", data);
        return changed;
    }

    [GtkCallback]
    async void on_layout_selected() {
        _ready = (picker.selected != null);
        this.notify_property("ready");
        if (!ready) return;

        if (picker.selected.kind == Kind.IBUS) {
            // First, reset to the English US layout. Some ibus input methods
            // behave differently depending on what keyboard layout was used
            // before they were activated, which leads to lots of confusion in
            // an initial-setup context (where picking different languages
            // can automatically change input methods). QWERTY is a good bet
            // for these input methods
            var changed = set_layout(Kind.XKB, "us");

            // Now we wait. If we do this too fast, we may end up switching
            // the input method before other components can finish updating
            // their own internal state, which leads to very inconsistent and
            // very confusing (UX-wise) race conditions where the fix above
            // may or may not work depending on random chance. This gets worse
            // in the greeter, where we're using dbus-daemon rather than
            // dbus-broker, and it's particularly egregious in VMs on slow hw
            //
            // Example: Going from Russian to Russian Translit (m17n) may work
            // (xkb:us was applied fast enough) or do nothing (it wasn't)
            if (changed) {
                Timeout.add(500, on_layout_selected.callback);
                yield;
            }
        }

        set_layout(picker.selected.kind, picker.selected.id);
    }

    [GtkCallback]
    void on_next() {
        this.next();
    }

    bool apply() {
        var layout = "us", variant = "";
        if (picker.selected.kind == Kind.XKB) {
            layout = picker.selected.xkb_layout;
            variant = picker.selected.xkb_variant;
        }
        set_localed_kbd.begin(layout, variant);

        return true;
    }
}
