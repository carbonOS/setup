[GtkTemplate(ui="/sh/carbon/setup/ui/password.ui")]
class Setup.PasswordPage : Adw.Bin, Setup.Page {
    public bool ready { get { return validate(); } }

    private Driver driver;

    [GtkChild] unowned Gtk.PasswordEntry password;
    [GtkChild] unowned Gtk.PasswordEntry confirm;

    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Gtk.Label password_lbl;
    [GtkChild] unowned Gtk.Label confirm_lbl;

    public PasswordPage(Driver driver) {
        this.driver = driver;
    }

    void populate_text() {
        title.label = _("Set a Password");
        password_lbl.label = _("Password");
        // TRANSLATORS: Tells user to enter their password again to confirm it
        confirm_lbl.label = C_("Password", "Confirm");
    }

    [GtkCallback]
    private void invalidate_ready() {
        this.notify_property("ready");
    }

    [GtkCallback]
    private void maybe_next() {
        if (ready) next();
    }

    private bool validate() {
        // Bail if there's no password available
        if (password.text.length == 0) return false;

        // TODO: Check the password quality

        // Bail if there's no confirmation
        if (confirm.text.length == 0) {
            return false;
        }

        // Check if the password and the confirm match
        if (password.text != confirm.text) {
            password.add_css_class("error");
            confirm.add_css_class("error");
            return false;
        } else {
            password.remove_css_class("error");
            confirm.remove_css_class("error");
            return true;
        }
    }

    bool apply() {
        driver.new_user.password = password.text;
        return true;
    }
}

// TODO: Implement libpwquality
/*
private Gtk.Label tip;
private Gtk.LevelBar pw_quality;
private PasswordQuality.Settings? pwq;

pw_quality = new Gtk.LevelBar();
pw_quality.max_value = 5;
pw_quality.mode = Gtk.LevelBarMode.DISCRETE;
grid.attach(pw_quality, 1, 1);

tip = new Gtk.Label("");
tip.margin_bottom = 6;
grid.attach(tip, 1, 2);

// Init libpwquality
this.pwq = new PasswordQuality.Settings();
void* pwq_aux = null;
PasswordQuality.Error pwq_error = pwq.read_config(null, out pwq_aux);
if (pwq_error < 0) {
    critical("Couldn't init pwquality: %s", pwq_error.to_string(pwq_aux));
    pw_quality.visible = false;
    pwq = null;
}
*/
