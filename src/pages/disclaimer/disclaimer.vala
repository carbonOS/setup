class Setup.DisclaimerPage : Adw.Bin, Setup.Page {
    public bool ready { get { return true; } }

    private Gtk.Label title;
    private Gtk.Label subtitle;

    construct {
        var clamp = new Adw.Clamp();
        clamp.maximum_size = 400;
        clamp.tightening_threshold = 200;
        clamp.valign = Gtk.Align.CENTER;
        this.set_child(clamp);

        var root = new Gtk.Box(Gtk.Orientation.VERTICAL, 6);
        clamp.set_child(root);

        var icon = new Gtk.Image.from_icon_name("dialog-warning-symbolic");
        icon.pixel_size = 90;
        icon.margin_bottom = 6;
        icon.add_css_class("warning");
        root.append(icon);

        title = new Gtk.Label(null);
        title.add_css_class("title-1");
        root.append(title);

        subtitle = new Gtk.Label(null);
        subtitle.wrap = true;
        subtitle.justify = Gtk.Justification.CENTER;
        root.append(subtitle);
    }

    void populate_text() {
        var os_name = Environment.get_os_info(OsInfoKey.NAME);
        title.label = _("Disclaimer");
        // TRANSLATORS: The %s will be replaced with the name of the OS
        subtitle.label = _("This is an early development build of %s. " +
            "Please be aware that breaking changes may still occur. You may " +
            "need to manually migrate data after such a change. We provide no " +
            "stability or security guarantees. Back up all important data. Do " +
            "not use in a production environment.").printf(os_name);
    }
}
