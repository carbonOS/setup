using Gnome.Languages;

namespace Setup.Lang {
    // One locale in the LocaleModel
    private class Locale : Object {
        public string locale_id;

        // Display names
        public string lang_name;
        public string? country_name = null;

        // Search names
        public string? locale_name;
        public string? locale_current_name;
        public string? locale_untranslated_name;

        // Filter & sort
        public bool is_extra;
        public string? sort_key = null;

        public Locale(string id) {
            locale_id = id;

            string lang; // Name of the language (i.e. en)
            string? country; // Name of the region (i.e. US)
            parse_locale(id, out lang, out country, null, null);

            lang_name = get_language_from_code(lang, id) ??
                        get_language_from_code(lang, null);
            if (country != null) {
                country_name = get_country_from_code(country, id) ??
                               get_country_from_code(country, null);
            }

            is_extra = !(id in INITIAL_LOCALES);
            locale_name = get_language_from_locale(id, id);
            locale_current_name = get_language_from_locale(id, null);
            locale_untranslated_name = get_language_from_locale(id, "C");
            if (locale_name != null)
                sort_key = CC.normalize_casefold_and_unaccent(locale_name);
        }

        // Special handling for more button and empty label
        public bool is_more = false;
        public Locale.more_btn() {
           is_more = true;
        }
    }

    // A ListModel of all the locales on the system (with a more item)
    private class LocaleModel : ListModel, Object {
        private ListStore base_model;
        private Gtk.Filter filter;
        private Gtk.FilterListModel filter_model;

        private bool more_visible = false;
        private string filter_text = "";

        construct {
            // Create, populate, and sort the base model
            base_model = new ListStore(typeof(Locale));
            foreach (var l in get_all_locales())
                base_model.append(new Locale(l));
            base_model.sort((a, b) => sort_func(a as Locale, b as Locale));

            // Setup the filter
            filter = new Gtk.CustomFilter(it => filter_func(it as Locale));
            filter_model = new Gtk.FilterListModel(base_model, filter);

            // Delegate the items_changed signal to the filter_model
            filter_model.items_changed.connect((pos, rm, add) => {
                this.items_changed(pos, rm, add);
            });
        }

        private bool filter_func(Locale item) {
            // Hide extra rows if more button wasn't clicked
            if (item.is_extra && !more_visible) return false;

            // Filter based on search box text
            if (item.locale_name != null &&
                filter_text.match_string(item.locale_name, true))
                return true;
            if (item.locale_current_name != null &&
                filter_text.match_string(item.locale_current_name, true))
                return true;
            if (item.locale_untranslated_name != null &&
                filter_text.match_string(item.locale_untranslated_name, true))
                return true;
            return false;
        }

        private int sort_func(Locale a, Locale b) {
            // Extra languages always go after initial languages
            if (a.is_extra && !b.is_extra) return 1;
            if (!a.is_extra && b.is_extra) return -1;

            // Sort by sort key if available
            if (a.sort_key != null && b.sort_key != null) {
                var sort_key = strcmp(a.sort_key, b.sort_key);
                if (sort_key != 0) return sort_key;
            }

            // Fallback to sort by locale ID
            return strcmp(a.locale_id, b.locale_id);
        }

        Type get_item_type() {
            return typeof(Locale);
        }

        uint get_n_items() {
            return filter_model.get_n_items() + (more_visible ? 0 : 1);
        }

        Object? get_item(uint pos) {
            if (pos < filter_model.get_n_items())
                return filter_model.get_item(pos);
            return new Locale.more_btn();
        }

        // Shows the whole language list and hides the more button
        public void show_more() {
            items_changed(get_n_items() - 1, 1, 0); // Delete the more item
            this.more_visible = true;
            filter.changed(Gtk.FilterChange.LESS_STRICT);
        }

        // Filters the language list
        public void update_filter(string text) {
            this.filter_text = text;
            filter.changed(Gtk.FilterChange.DIFFERENT);
        }
    }

    // One language in the language picker widget
    [GtkTemplate(ui="/sh/carbon/setup/ui/locale-row.ui")]
    class LocaleRow : Gtk.ListBoxRow {
        private Picker picker;
        public Locale locale;

        [GtkChild] unowned Gtk.Label lang_label;
        [GtkChild] unowned Gtk.Label country_label;
        [GtkChild] unowned Gtk.Image checkmark;

        public LocaleRow(Picker picker, Locale locale) {
            this.picker = picker;
            this.locale = locale;
            lang_label.label = locale.lang_name;
            country_label.label = locale.country_name;
            picker.notify["selected-locale"].connect(update_check);
            update_check();
        }

        private void update_check() {
            var selected = (picker.selected_locale == locale.locale_id);
            checkmark.opacity = selected ? 1 : 0;
        }
    }

    // Language picker widget
    [GtkTemplate(ui="/sh/carbon/setup/ui/language-picker.ui")]
    class Picker : Gtk.Box {
        private LocaleModel model;
        [GtkChild] unowned Gtk.Entry search_box;
        [GtkChild] unowned Gtk.ListBox list_box;
        [GtkChild] unowned Gtk.Label empty_state;
        [GtkChild] unowned Gtk.Image more_row;
        [GtkChild] unowned Gtk.Stack placeholder;
        [GtkChild] unowned Gtk.Revealer search_revealer;

        public string selected_locale { get; set; }

        construct {
            model = new LocaleModel();
            model.items_changed.connect(on_model_changed);
            on_model_changed();
            list_box.bind_model(model, it => create_row(it as Locale));
        }

        public void populate_text() {
            // TRANSLATORS: Found here means that the user searched for a
            // language that isn't in the list.
            empty_state.label = _("No Languages Found");
            search_box.placeholder_text = _("Search");
            more_row.tooltip_text = _("More…");
        }

        private Gtk.Widget create_row(Locale l) {
            if (l.is_more) return more_row;
            return new LocaleRow(this, l);
        }

        private void on_model_changed() {
            if (model.get_n_items() == 0)
                placeholder.visible_child = empty_state;
            else
                placeholder.visible_child = list_box;
        }

        [GtkCallback]
        private void on_search_changed() {
            model.update_filter(search_box.text);
        }

        [GtkCallback]
        private void on_activated(Gtk.ListBoxRow row) {
            if (row.child == more_row) { // Handle the more item
                search_revealer.reveal_child = true;
                search_box.grab_focus();
                model.show_more();
                return;
            }

            var id = ((LocaleRow) row).locale.locale_id;
            if (selected_locale != id) {
                selected_locale = id;
            } else confirm();
        }

        public signal void confirm();
    }
}
