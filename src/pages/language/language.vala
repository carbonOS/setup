[GtkTemplate(ui="/sh/carbon/setup/ui/language.ui")]
class Setup.LanguagePage : Adw.Bin, Setup.Page {
    private bool _ready = false;
    public bool ready { get { return _ready; } }

    private Driver driver;
    [GtkChild] unowned Gtk.Stack welcome;
    [GtkChild] unowned Lang.Picker picker;
    private uint visible_child = 0;
    private uint welcome_timer = 0;

    public LanguagePage(Driver driver) {
        this.driver = driver;
        setup_welcome();
    }

    void populate_text() {
        picker.populate_text();
    }

    private string translate_welcome(string lang) {
        var new_locale = Lang.newlocale(Lang.LC_MESSAGES_MASK, lang, null);
        if (new_locale == null) return "Welcome!";

        var old_locale = Lang.uselocale(new_locale);
        // TRANSLATORS: This should be a warm, welcoming message. Someething
        // you'd say to greet someone at the door
        var translated = _("Welcome!");

        Lang.uselocale(old_locale);
        Lang.freelocale(new_locale);
        return translated;
    }

    private void setup_welcome() {
        string[] seen_locales = {};
        foreach (var lang in Lang.INITIAL_LOCALES) {
            var text = translate_welcome(lang);
            if (text in seen_locales) continue;
            seen_locales += text;

            var label = new Gtk.Label(text);
            label.add_css_class("title-1");
            welcome.add_named(label, lang);
        }
    }

    void enter() {
        // Handle the welcome animation
        if (welcome_timer != 0) return;
        welcome_timer = Timeout.add_seconds(4, cycle_welcome);
    }

    bool cycle_welcome() {
        visible_child++;
        visible_child %= Lang.INITIAL_LOCALES.length;
        welcome.visible_child_name = Lang.INITIAL_LOCALES[visible_child];
        return Source.CONTINUE;
    }

    [GtkCallback]
    void on_lang_selected() {
        driver.set_language(picker.selected_locale);
        _ready = true;
        this.notify_property("ready");
    }

    [GtkCallback]
    void on_next() {
        this.next();
    }

    bool apply() {
        // Stop the welcome spinner
        Source.remove(welcome_timer);
        welcome_timer = 0;

        // Tell logind about the locale
        Lang.set_localed_locale.begin(picker.selected_locale);
        return true;
    }
}
