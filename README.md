# carbonOS Setup

Provides the setup experience for carbonOS, including installing the OS, 
configuring basic settings and creating users. Also acts as a lightdm greeter
to log the system into the new user immediately after it is created

## TODO

- Make password page skippable
- Make wifi picker work in greeter session
- User-mode initial-setup
- Repair in installer
- Handle installer errors better
- Translate the .desktop entry
- https://github.com/endlessm/gnome-initial-setup/blob/master/data/20-gnome-initial-setup.rules.in

## Building

Development is easiest on carbonOS through GNOME Builder. Simply switch carbonOS
to the devel variant:

```bash
$ updatectl switch --base=devel
[Enter your credentials at the prompt]
[Reboot]
```

set the `SETUP_MODE` runtime environment variable in Builder, and hit run

## Modes

Setup has a couple of modes it starts in, depending on its environment:

- `initial-setup`: This does the basic setup steps required to start using
carbonOS, like picking a language, picking a timezone, and creating a user. This
mode is selected when Setup is running in LightDM and the system is not a LiveOS
- `installer`: Select a disk, wipe it, and install carbonOS. This mode is selected
when Setup is running outside of LightDM and the system is a LiveOS
- `installer-greeter`: Just like `installer`, but it lets you pick a language
and then pick whether to install or try the OS. This mode is selected when Setup
is running in LightDM and the system is a LiveOS

## Testing

When `SETUP_MODE` is set, Setup goes into "mock" mode and no changes are made
to the system. `SETUP_MODE` also determines what mode Setup is in. You can
forcibly disable mock mode by running `SETUP_NO_MOCK=1`. If Setup is in mock mode,
the headerbar has the standard "devel art" as its background

## Icon Credits

This project's icon was obtained from [Peter Eisenmann's os-installer](https://gitlab.gnome.org/p3732/os-installer)
